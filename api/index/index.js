import {
	http,
	httpNoAuth
} from '@/api/httputil.js'

/**
 * 获取今天天气信息
 */
export const getWeatherInfo = () => {
	return http.get('/prod-api/api/common/weather/today');
}

/**
 * 获取一周信息
 */
export const getWeekWeatherInfo = () => {
	return http.get('/prod-api/api/common/weather');
}
/**
 * 获取新闻列表
 */
export const getNewsListInfo = () => {
	return http.get('/prod-api/press/press/list');
}

export const getServiceInfo = () => {
	let data = http.get('/prod-api/api/living/category/list');
	return data;
}


/**
 * 获取电费缴费的历史记录
 */
export const getFeeChargeHistory = () => {
	let data = http.get('/prod-api/api/living/recharge/record/list?categoryId=3&pageNum=1&pageSize=50');
	console.log(data);
	return data;
}

/**
 * 电费缴费接口
 */
export const chargeFee = (chargeType) => {
	return http.post('/prod-api/api/living/recharge', {
		billNo: "202104240810306",
		paymentNo: "15670226",
		paymentType: chargeType
	});
}


/**
 * 查询生活资讯接口
 */
export const getNewsInfo = (pageSize, pageNum) => {
	return http.get("/prod-api/api/living/press/press/list?type=26&pageNum=" + pageSize + "&pageSize=" + pageNum);
}

/**
 * 获取新闻评论接口
 */
export const getNewsCommet = (newsId) => {
	return http.get("/prod-api/api/living/press/comments/list?newsId=" + newsId);
}
/**
 * 提交评论
 */
export const submitCommet = (newsId, newsContent) => {
	return http.post("/prod-api/api/living/press/pressComment", {
		newsId: newsId,
		content: newsContent
	});
}
/**
 * 点赞文章,使用es6中的字符串
 */
export const likeArticle = (newsId) => {
	return http.put(`/prod-api/api/living/press/press/like/${newsId}`);
}

/**
 * 点赞评论
 */
export const likeCommet = (commetId) => {
	return http.put(`/prod-api/api/living/press/pressComment/like/${commetId}`);
}

/**
 * 登录
 */
export const doLogin = (name, pass) => {
	return httpNoAuth.post('/prod-api/api/login', {
		username: name,
		password: pass
	});
}
/**
 * 获取咨询详情
 */
export const newsDetail = (newsId) => {
	return http.get(`/prod-api/api/living/press/press/${newsId}`);
}
/**
 * 获取个人信息
 */
export const personInfo = () => {
	return http.get('/prod-api/api/common/user/getInfo');
}

/**
 * 修改除头像外信息
 */
export const modifyPersonInfo = (personInfo) => {
	return http.put('/prod-api/api/common/user', {
		email: personInfo.email,
		nickName: personInfo.name,
		phonenumber: personInfo.number,
		sex: personInfo.sex
	});
}

/**
 * 修改头像信息
 */
export const modifyAvator = (imgUrl) => {
	return http.put('/prod-api/api/common/user', {
		avatar: imgUrl
	});
}


/**
 * 提交意见
 */
export const submitMessage = (message) => {
	return http.post('/prod-api/api/living/feedback', {
		content: message
	});
}
/**
 * 获取意见列表
 */
export const submitList = () => {
	return http.get('/prod-api/api/living/feedback/list');

}
/**
 * 上传文件到服务器，第一次写参数写错
 */
export const upLoadFile = (path) => {
	return http.upload('/prod-api/common/upload', {
		name: 'file',
		filePath: path
	});
}
