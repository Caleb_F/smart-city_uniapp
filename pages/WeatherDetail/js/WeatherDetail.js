const chartData = (data) => {
	var data1 = [];
	//返回处理好的数据
	data.today.hours.forEach(item => {
			let message={}
			message.text=item.hour
			message.value=item.temperature
			message.group="今日气温详情"
			data1.push(message)
	});
	return data1;
}

const weekChartData=(data) =>{
	var weekChartData=[];
	/**
	 * 湿度数据
	 */
	data.weatherList.forEach(item =>{
		let humData={};
		humData.value=item.humidity
		humData.text=item.day
		humData.group="湿度详情"
		weekChartData.push(humData)
	})
	/**
	 * 温度数据
	 */
	data.weatherList.forEach(item =>{
		let humData={};
		humData.value=item.temperature
		humData.text=item.day
		humData.group="温度详情"
		weekChartData.push(humData)
	})
	/**
	 * 平均温度数据
	 */
	data.weatherList.forEach(item =>{
		let humData={};
		humData.value=item.apparentTemperature
		humData.text=item.day
		humData.group="平均温度详情"
		weekChartData.push(humData)
	})
	return weekChartData;
	
}





export {
	chartData,weekChartData
}

